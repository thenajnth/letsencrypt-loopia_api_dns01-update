LET'S ENCRYPT WITH LOOPIA DNS API
=================================

Detta lilla script uppdaterar Let's encrypt certifikat via DNS. I mitt
fall är det nödvändigt eftersom webbservern körs på alternativ
https-port.

# Beroenden
- Git (för installation)
- Python 3
- certbot

# Installation

Skriptet behöver ingen installation. Det räcker med att du klonar
git-förrådet det ligger i. Men om du vill köra detta på en
Debian-/Ubuntu-installation så kanske du skulle göra ungefär så här:

    $ sudo -s
    [sudo] password for username:

    # cd /root
    # git clone https://thenajnth@bitbucket.org/thenajnth/letsencrypt-loopia_api_dns01-update.git

# Inställningar

## I Loopias kundzon
- Lägg till en domänpost på ditt loopia-konto av typen TXT för en subdomän till den subdomän det
  gäller (du gör detta i DNS-editorn). Om du har en subdomän som är www, så lägg till en TXT-post för
  _acme-challenge.www
- Kolla nu i HTML-koden för dnseditor-filen så ser du att TXT-posten ligger i
  en div med id:t `zone_record_xxxxxxxx` där x:en är det nummer som i
  inställningsfilen nedan kallas "RecordId".

(Jag är medveten om att det förmodligen finns ett mycket bättre sätt
att se RecordId, men jag har inte gjort några efterforskningar.)

## Inställningsfil för skript

Inställningarna liger i en fil vid namn `update-letsencrypt-settings.ini`
i användaren som kör skriptets hemkatalog (dvs.
`~/update-letsencrypt-settings.ini`). Det är enkelt
att byta namn eller plats på filen genom att ändra i konstanterna i båda
python-filerna.

Här följer ett exempel på hur filen kan se ut:

    [LETS_ENCRYPT]
    Email = användare@domän.se
    FQDN = www.domän.se
    CertbotPath = /usr/bin/certbot-auto

    [LOOPIA_API]
    DomainServerUrl = https://api.loopia.se/RPCSERV
    Username = loopia-användare
    Password = loopia-lösenord
    Domain = domän.se
    Subdomain = www
    RecordId = xxxxxxxxx
    
### Användning

Kör filen `update_letsencrypt.py`. Det skulle exempelvis
kunna se ut så här:

    $ sudo -s
    [sudo] password for username:
    
    # cd /root/letsencrypt-loopia_api_dns01-update
    # ./update_letsencrypt.py
    
Detta genererar nycklarna som webbservern sedan behöver. Du kan
modifiera kommandoraden i `update_letsencrypt.py` om du exempelvis
automatiskt vill uppdatera apache-filerna.

Men i verkligheten vill du förstås förmodligen köra detta via ett
cron-jobb.
