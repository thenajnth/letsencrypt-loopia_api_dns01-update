#!/usr/bin/env python3  
# -*- coding: utf-8 -*-  

"""
This file should be run as a hook to certbot.
"""

import os

import configparser
import xmlrpc.client

CONFIG_FILE = os.path.expanduser('~/update-letsencrypt-settings.ini')
SECTION = 'LOOPIA_API'  # Section of ini-file


def update_zone_record(settings, validation_string):
    client = xmlrpc.client.ServerProxy(uri=settings['DomainServerUrl'], encoding='utf-8')

    record = {
        'type': 'TXT',
        'ttl': '300',
        'priority': '0',
        'rdata': validation_string,
        'record_id': settings['RecordId']
    }

    response = client.updateZoneRecord(settings['Username'], settings['Password'],
                                       settings['Domain'], '_acme-challenge.' + settings['Subdomain'],
                                       record)
    print(('Response: {}.'.format(response)))


if __name__ == '__main__':
    config = configparser.ConfigParser()
    config.read(CONFIG_FILE)
    settings = config[SECTION]

    validation_string = os.environ['CERTBOT_VALIDATION']

    update_zone_record(settings, validation_string)
