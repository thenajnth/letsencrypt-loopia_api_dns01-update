#!/usr/bin/env python3

"""
Run this file to update Let's Encrypt.
"""

import configparser
import os
import subprocess

CONFIG_FILE = os.path.expanduser('~/update-letsencrypt-settings.ini')

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))  # Path of current file
AUTH_SCRIPT = os.path.join(SCRIPT_DIR, 'authenticate_loopia_api.py')

SECTION = 'LETS_ENCRYPT'  # Section of ini-file


def update_cert(certbot_path, auth_script, fqdn, email):
    cmd = [certbot_path, 'certonly', '--manual', '--preferred-challenges=dns',
           '--manual-auth-hook', auth_script, '-d', fqdn, '-m', email]
    subprocess.call(cmd)


if __name__ == '__main__':
    config = configparser.ConfigParser()
    config.read(CONFIG_FILE)
    settings = config[SECTION]

    update_cert(settings['CertbotPath'], AUTH_SCRIPT, settings['FQDN'], settings['Email'])

